resource "aws_instance" "bastion" {
  ami                         = var.image_ami
  associate_public_ip_address = true
  disable_api_termination     = true
  instance_type               = var.instance_type
  key_name                    = "bastion"
  vpc_security_group_ids      = [aws_security_group.this.id]
  subnet_id                   = element(tolist(data.aws_subnet_ids.public.ids), 0)

  tags = {
    Name    = "${var.name}-bastion"
    Product = var.name
  }

  volume_tags = {
    Name    = "${var.name}-bastion-volume"
    Product = var.name
  }
}
